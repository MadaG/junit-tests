#####ALM Octane GitLab CI service#####
**_This service integrates ALM Octane with GitLab, enabling ALM Octane to display GitLab build pipelines and track build and test run results._**

Integration flow:
The service communicates both with the GitLab server and the Octane server. Both sides of communication must be reachable.

Communication with Gitlab:
The service sends an API requests to GitLab server.
For example: check permission, get project list, get test results, run project.

GitLab sends events to this service.
For example: start and finish running of project.
The service registers to events using the GitLab project webhook mechanism.

**Note that if the service is down or unavailable, the data will be lost and not displayed in ALM Octane.**
******************************************************************************************************

